_meta:
  version: '1.0'
  entity_type: node
  uuid: 82c551f2-4cdd-4429-a1c0-e05ffafe37a6
  bundle: use_case
  default_langcode: en
  depends:
    44972a58-15ce-483a-814f-77d902225471: media
default:
  revision_uid:
    -
      target_id: 1
  status:
    -
      value: true
  uid:
    -
      target_id: 1
  title:
    -
      value: 'Accelerated the Performance of API Management Platform by Productizing APIs'
  created:
    -
      value: 1637662469
  promote:
    -
      value: false
  sticky:
    -
      value: false
  revision_translation_affected:
    -
      value: true
  moderation_state:
    -
      value: published
  metatag:
    -
      tag: meta
      attributes:
        name: title
        content: 'Accelerated the Performance of API Management Platform by Productizing APIs | Ezdevportal'
    -
      tag: meta
      attributes:
        name: description
        content: 'Srijan helped a leading telecom giant in Asia to increase the capacity of their API gateway by improving the performance of their Apigee API management platform, and productize APIs - without changing the internal legacy systems'
    -
      tag: link
      attributes:
        rel: canonical
        href: 'https://ezp.ddev.site/use-case/accelerated-performance-api-management-platform-productizing-apis'
  path:
    -
      alias: /use-case/accelerated-performance-api-management-platform-productizing-apis
      langcode: en
      pathauto: 1
  body:
    -
      value: "<h2>Highlights:</h2>\r\n\r\n<ul>\r\n\t<li>Created new revenue streams by adding two new API products</li>\r\n\t<li>Built a priority queuing mechanism to improve response time for time-critical messages</li>\r\n\t<li>Solved delayed SMS issues, which led to the reduction in support tickets for SMS delays</li>\r\n</ul>\r\n\r\n<p>Even with API Gateways in place, consumer-facing industries like Telecom and BFSI face pain points in managing their API programs effectively. One of the major reasons for this is the presence of legacy internal systems. With the exponential shift into APIs, these old systems become a bottleneck as they are not designed to handle the huge volume of transactions that are bound to happen via API calls.</p>\r\n\r\n<p>Building API strategy with a product mindset is one of the best ways to solve the performance issues that crop up in legacy environments, without having to make changes in the internal systems. Srijan Technologies has encountered its fair share of customers who have faced similar challenges and has successfully solved for scalability and performance.</p>\r\n\r\n<p>Our client, a major telecom service provider in the Philippines, was facing capacity and performance issues with their SMS API. The pain point was that the API developers were facing a delay of more than 5 hours between the API calls and the SMS sent to the end-users.</p>\r\n\r\n<h2>Requirements</h2>\r\n\r\n<p>The customer wanted to increase the throughput of SMS API’s capacity and reduce the time taken between the API calls and the SMS being sent to the end-users.</p>\r\n\r\n<h2>Challenge</h2>\r\n\r\n<p>Our customer was facing high volumes of support issues/complaints on slow delivery of SMS, and developers who build time-sensitive SMS were affected the most. Their Apigee API Platform handled a considerable amount of SMS throughput, but the capacity of the API Gateway was throttled due to the legacy infrastructure. The customer wanted to solve this problem without touching their internal core systems.</p>\r\n\r\n<h2>Solution</h2>\r\n\r\n<p>Srijan’s engineering team took a consultative standpoint to solve this issue by a multi-pronged approach, which is given below.</p>\r\n\r\n<ul>\r\n\t<li>Started off by identifying the root cause of the SMS traffic choke</li>\r\n\t<li>To reduce the system loads, recommended to distribute/spread the load on the internal systems by defining priority queues across all API products</li>\r\n\t<li>Build a new API Product that can handle Bulk SMS</li>\r\n</ul>\r\n\r\n<p>With this approach, the customer will be able to scale their API programs without having to make changes to the core system.</p>\r\n\r\n<h2>Overall Approach</h2>\r\n\r\n<p>Based on our interaction with our customer’s business teams we found that SMS API was consumed by the developers to trigger any type of SMS, including bulk messages. On analyzing the SMS API’s traffic data, we identified that broadcast type messages that were native to marketing campaigns were causing spikes in the SMS traffic. To overcome this problem, we did the following:</p>\r\n\r\n<ul>\r\n\t<li><strong>API productization approach to solve the delayed SMS issue</strong><br />\r\n\tFor the benefit of API consumers (developers) who build the bulk SMS functionality, we built a separate API. Having a separate API product for this niche was well received as it can be easily consumed by the developer community. This new API product will remove the bulk message traffic from the SMS API, thus reducing the time taken to deliver SMS to the end customer.</li>\r\n\t<li><strong>Distribute / Spread the load on the internal systems</strong><br />\r\n\tTo avoid uneven loads, we audited the load distribution on the internal systems and defined a redistribution mechanism to manage load balancing via API gateways. This helped increase the capacity of the API gateway by about 10X.</li>\r\n\t<li><strong>Introduce a Priority API product queue</strong><br />\r\n\tTo avoid delays in sending time-sensitive SMS, we added a queuing mechanism for all the API products and introduced ‘Priority Queues’ for dedicated segments of APIs. This has helped increase the capacity of the API Platform massively, and decreased the amount of support tickets.</li>\r\n</ul>\r\n\r\n<p>Here is an overview diagram of our approach:</p>\r\n\r\n<p><img alt=\"Overall approach\" data-entity-type=\"file\" data-entity-uuid=\"16c7c9ab-c453-47c8-a139-affbe05ef615\" src=\"/sites/default/files/inline-images/Diagram-final_0.png\" /></p>\r\n\r\n<h2>Tech Stack</h2>\r\n\r\n<ul>\r\n\t<li>API Platform - Google Apigee</li>\r\n\t<li>Microservices in NodeJS</li>\r\n\t<li>Infrastructure as Code on AWS</li>\r\n\t<li>Fully containerized deployments on Kubernetes</li>\r\n</ul>\r\n\r\n<h2>Business benefit</h2>\r\n\r\n<p>With 10X increase in API throughput, prioritized queuing mechanism for better load balancing and API productization, there was a significant reduction of time taken between API calls and SMS delivery.&nbsp;<br />\r\n<br />\r\nFurther, we are engaged with the telecom client on a long term and help them in scaling their data monetization activities, which is built on a partner &amp; open API ecosystem, federating access to multi million subscribers across Telco 2.0 APIs featuring Identity, Location, Payments, Digital Services and more.</p>\r\n"
      format: full_html
      summary: 'Srijan helped a leading telecom giant in Asia to increase the capacity of their API gateway by improving the performance of their Apigee API management platform, and productize APIs - without changing the internal legacy systems'
  field_listing_image:
    -
      entity: 44972a58-15ce-483a-814f-77d902225471
