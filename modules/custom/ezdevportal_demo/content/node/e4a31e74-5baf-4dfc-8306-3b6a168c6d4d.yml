_meta:
  version: '1.0'
  entity_type: node
  uuid: e4a31e74-5baf-4dfc-8306-3b6a168c6d4d
  bundle: use_case
  default_langcode: en
  depends:
    44972a58-15ce-483a-814f-77d902225471: media
default:
  revision_uid:
    -
      target_id: 1
  status:
    -
      value: true
  uid:
    -
      target_id: 1
  title:
    -
      value: 'Building a Fintech Sandbox Open Banking Ecosystem With APIGEE API Management'
  created:
    -
      value: 1637662415
  promote:
    -
      value: false
  sticky:
    -
      value: false
  revision_translation_affected:
    -
      value: true
  moderation_state:
    -
      value: published
  metatag:
    -
      tag: meta
      attributes:
        name: title
        content: 'Building a Fintech Sandbox Open Banking Ecosystem With APIGEE API Management | Ezdevportal'
    -
      tag: meta
      attributes:
        name: description
        content: 'Building a fintech ecosystem atop core banking services, with an automated pipeline for converting existing SOAP services to REST APIs, API management with Apigee, and Drupal-based developer portal.'
    -
      tag: link
      attributes:
        rel: canonical
        href: 'https://ezp.ddev.site/use-case/building-fintech-sandbox-open-banking-ecosystem-apigee-api-management'
  path:
    -
      alias: /use-case/building-fintech-sandbox-open-banking-ecosystem-apigee-api-management
      langcode: en
      pathauto: 1
  body:
    -
      value: "<h2>Highlights:</h2>\r\n\r\n<ul>\r\n\t<li><strong>Goal:&nbsp;</strong>Creating a fintech sandbox to expose client APIs to third-party developers&nbsp;</li>\r\n\t<li><strong>Solution:&nbsp;</strong>Building automated pipeline for converting WSDL files to REST APIs, and onboarding to sandbox</li>\r\n\t<li><strong>Outcome:</strong>&nbsp;A functioning API ecosystem, enabling better adoption of client APIs with a easy-to-use developer dashboard</li>\r\n</ul>\r\n\r\n<p>Open banking is becoming an emerging trend for the BFSIs owing to the changing customer needs, Fintech competition, as well as GDPR regulations. It provides them with the opportunity to improve, inform, and further the value of their analytics and data securely, while also driving customer engagement and an increase in revenue.</p>\r\n\r\n<p>The client wanted to extend the services to their clients by introducing native FinTech solutions and opening up the YesBank API to other third-party apps and fintech partners in a plug-and-play manner.&nbsp;<br />\r\n<br />\r\nThis was to ensure that routine interactions were made easy and convenient by modernizing the customers’ experience where instead of relying solely on client’s channels, customers could now transact through other apps.&nbsp;<br />\r\n<br />\r\nSrijan helped the client identify a key element for success was to create a great experience for the developers. Here’s how we helped the client bring ease and speed of getting services to the market.</p>\r\n\r\n<h2>The Challenge</h2>\r\n\r\n<p>To have an edge over their competitors, Yes Bank wanted to build a fintech ecosystem atop its core banking services. In order to achieve this goal, the client had to reinvent their API strategy.&nbsp;<br />\r\n<br />\r\nCertain challenges that came in the way were:</p>\r\n\r\n<ul>\r\n\t<li>Lack of CMS with APIGEE</li>\r\n\t<li>Rolling out new features and services with a shorter turnaround time</li>\r\n\t<li>Maintain the technical hierarchy without impacting the security</li>\r\n\t<li>With both internal and external developers working, it was imperative that they had an understanding of each of the services<br />\r\n\t&nbsp;</li>\r\n</ul>\r\n\r\n<h2>The Solution</h2>\r\n\r\n<p>Given the requirements of the project, the solution proposed had the following key aspects:</p>\r\n\r\n<ul>\r\n\t<li><strong>Elevating developer experience with Drupal</strong></li>\r\n</ul>\r\n\r\n<p>With an API provider, one needs to expose the APIs, educate their developers about the API to create an understanding of the project.&nbsp;<br />\r\n<br />\r\nAPIGEE doesn’t have CMS, which makes it difficult to collate and collaborate. Without any content UI, developers who wanted to look at the APIs and related information needed to reach out to our respective teams and go through a very manual and time-consuming process to get started. Integrating Drupal helped cut-short the time.&nbsp;<br />\r\n<br />\r\nDrupal provided a frontend for the documentation of APIs and codes, URLs for the code documentation pages, and acted as the endpoint of APIGEE.</p>\r\n\r\n<ul>\r\n\t<li><strong>Updating codes and changes in real-time</strong></li>\r\n</ul>\r\n\r\n<p>The architecture worked by pulling endpoints from APIGEE with Drupal. This provided a way for the developers to provide feedback, make support and feature requests, and submit their own content that could be accessed by other developers, which was synced with the APIGEE platform. The API management platform enabled different tiers of offerings by bundling APIs into different products.</p>\r\n\r\n<ul>\r\n\t<li><strong>A secure platform</strong></li>\r\n</ul>\r\n\r\n<p>The developer portal would allow third-party developers to work with these APIs and access all published documentation without them accessing the APIGEE interface. This added an extra layer of protection, defending the API ecosystem from any possible manual error.&nbsp;<br />\r\n<br />\r\nThe customized Drupal developer portal supported community and social features around the API portal to enable hackathons and API product summits.<br />\r\n<br />\r\nBy integrating Drupal with the API ecosystem, YesBank made the process and experience smoother for developers to roll out the API packages faster, minus the hassle that can sometimes go along with getting started with APIs.</p>\r\n\r\n<p><img alt=\"Overall approach\" data-entity-type=\"file\" data-entity-uuid=\"16f279a4-8ab9-425a-8794-c8a58c32a7f1\" src=\"/sites/default/files/inline-images/Fintech-Sandbox.png\" /></p>\r\n\r\n<h2>Business Benefits</h2>\r\n\r\n<p><strong>* Increasing the API footprint</strong>&nbsp;<br />\r\n<br />\r\nOnce the developer portal was integrated, YesBank experienced its partner ecosystem benefiting from it and growing, too. The client is providing its APIs via sandbox environments, allowing third-party apps to consume their APIs and/or partner with them for product development.<br />\r\n<br />\r\n<strong>* Lesser time to market</strong><br />\r\n<br />\r\nThe developer portal increased the reusability of solutions, while also bringing ease and speed of getting services to the market, reducing the time as much by 20%. Drupal not only helped to achieve YesBank’s go-to-market goals—it enhanced our capacity to capture new and unique use cases across multiple channels.&nbsp;<br />\r\n<br />\r\n<strong>Benefits</strong></p>\r\n\r\n<ul>\r\n\t<li>Safe and secure developer platform</li>\r\n\t<li>Simple to use and endlessly scalable APIs</li>\r\n\t<li>Concurrent testing and development to fast track app development cycle and reduce time to market</li>\r\n\t<li>Lesser time spent on training or hiring new specialized resources in house for the job</li>\r\n\t<li>End to end API management on the Apigee platform, thus ensuring a well-functioning API ecosystem in place</li>\r\n\t<li>Enabled better adoption and strategy of the client-side APIs with an easy-to-use developer dashboard</li>\r\n</ul>\r\n\r\n<p>Not only does the platform provide frictionless access, but it aims to modernize and simplify the way the client engaged with the developers and customers.</p>\r\n\r\n<p>Srijan is working with leading enterprises in the US, Europe, and APAC regions, assisting with their&nbsp;<a href=\"https://www.srijan.net/api-management-automation/api-management-solutions\" rel=\" noopener\">API lifecycle management</a>&nbsp;- creating and exposing APIs and building custom developer portals.</p>\r\n\r\n<p>Our teams also work closely with digital experience leaders to create a tailor-made API monetization strategy to enable enterprises to leverage the growing digital economy.</p>\r\n"
      format: full_html
      summary: 'Building a fintech ecosystem atop core banking services, with an automated pipeline for converting existing SOAP services to REST APIs, API management with Apigee, and Drupal-based developer portal.'
  field_listing_image:
    -
      entity: 44972a58-15ce-483a-814f-77d902225471
